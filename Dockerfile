# This is an Akumen Dockerfile.

# Dockerfiles can be used to set up the execution environment that your model runs in.
# You can use it to install additional drivers, software, read data, or many other things.
# This will be built before the first run of the model and cached, so you may experience
# some extra time to execute initially.

# Here, you can inherit from a base image. This is our standard execution image, so anything
# additional that you add to this file will build on top of our base execution environment.
# You can also specify your own base image! You just need to ensure that "python3" can be 
# executed from the command line, and that the Go standard runtime is installed.
FROM quay.io/optika.solutions/akumen-python:3.7.2

RUN apt-get install build-essential swig

# If you want to install additional Python packages, you can do this:
RUN pip3 install dask==2.30.0 distributed==2.30.1
RUN pip3 install auto-sklearn==0.11.1

# To set up environment variables, you can do this:
# ENV SOME_VARIABLE bob

# Note that any changes to the ENTRYPOINT or CMD values will be discarded.
# None of the other Dockerfile instructions really have any relevance, since the container
# will run for the duration of the model and then be discarded.
