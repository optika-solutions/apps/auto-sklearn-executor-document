import joblib
import requests
import base64
import autosklearn.classification
import sklearn.preprocessing
import pandas as pd
from akumen_api import AKUMEN_API_URL, API_KEY

# disable sklearn warnings
import warnings
warnings.filterwarnings('ignore') 


def akumen(training_model_name, target, data, **kwargs):
    """
    !! This akumen() function must exist in the execution file!

    Parameters:
        !! These lines define parameters, and a line must exist per input (or output).

        - Input: training_model_name [string]
        - Input: target [string]
        - Input: data [tabular] (csv)

        - Output: results [file] (results.csv)
    """
    print('Running Akumen model...')
    
    model_file = f'{training_model_name}.joblib.csv'.replace('\n', '').replace('\r', '')
    headers = { 'authorization': API_KEY }
    category = 'New'
    url = f'{AKUMEN_API_URL}documents/{category}/{model_file}'
    response = requests.get(url, headers=headers)
    obj = response.json()
    content = base64.b64decode(obj['content'])
    file = open('localfile', 'wb')
    file.write(content)
    file.close()

        
    automl = joblib.load('localfile')
    
    # pull out the feature names
    features = automl.akumen_features
    all_features = []
    for dtype in features.keys():
        all_features.extend(features[dtype])
        for key in features[dtype]:
            if dtype == 'numeric':
                data[key] = pd.to_numeric(data[key])
            elif dtype == 'datetime':
                data[key] = pd.to_datetime(data[key])
            elif dtype == 'categorical':
                data[key] = data[key].astype('category')
            else:
                raise Exception('Data type specified is not supported.')
    
    # make sure all features are present in the input dataset
    missing = []
    for feature in all_features:
        if feature not in data.columns:
            missing.append(feature)
    if missing:
        raise Exception('Some features are missing from the input data: ' + missing)
    
    labeller = automl.akumen_labeller
    
    data[kwargs.get('target', 'result')] = automl.predict(data)
    
    # invert the prediction
    data[kwargs.get('target', 'result')] = labeller.inverse_transform(data[kwargs.get('target', 'result')])

    # The akumen() function must return a dictionary including keys relating to outputs.
    return {
        'results': data
    }
